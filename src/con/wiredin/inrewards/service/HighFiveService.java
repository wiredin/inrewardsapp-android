package con.wiredin.inrewards.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import com.wiredin.inrewards.RedeemReceiptActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class HighFiveService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	String userId;
	String h5id;
	String time;

	public HighFiveService(Context context, String userId, String h5id) {
		this.context = context;
		this.userId = userId;
		this.h5id = h5id;
	}

	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
		// do initialization of required objects objects here
		Log.d("Hadian", "Running HighFive- Service id:" + userId + " viewedid:"
				+ h5id);
		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("Inrewards"); // title
		progressDialog.setMessage("Working.."); // message
		progressDialog.setCancelable(false);
		progressDialog.show();

	};

	@Override
	protected String doInBackground(Void... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(
				"http://rewards.wiredin.my/services/highFiveAction");
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("userid", this.userId));
			nameValuePairs.add(new BasicNameValuePair("viewedid", this.h5id));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

//	protected void onPostExecute(String results) {
//		if (results != null) {
//
//			Log.d("Hadian", "Jadi dgn result: " + results);
//			progressDialog.dismiss();
//
//			if (!results.equals("")) {
//				this.showSuccess();
//
//				GetUserService service = new GetUserService(context);
//				service.execute();
//				
//				time = results;
//
//			} else {
//				this.showError();
//			}
//		}
//	}

	public void showError() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		// black
		alertDialogBuilder.setInverseBackgroundForced(true);

		// set title
		alertDialogBuilder.setTitle("InRewards");

		// set dialog message
		alertDialogBuilder.setMessage("ada problem").setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public void showSuccess() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		// black
		alertDialogBuilder.setInverseBackgroundForced(true);

		// set title
		alertDialogBuilder.setTitle("InRewards");

		// set dialog message
		alertDialogBuilder.setMessage("High 5 Success! Thank you!")
				.setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
