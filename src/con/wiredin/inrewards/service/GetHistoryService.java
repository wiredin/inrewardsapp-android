package con.wiredin.inrewards.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.wiredin.inrewards.HistoryActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.History;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class GetHistoryService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	DatabaseHandler dbHandler;
	
	public GetHistoryService(Context context){
		this.context = context;
		dbHandler = new DatabaseHandler(context, null, null, 2);
	}
	
	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}
	
	@Override
    protected void onPreExecute()
    {
        
		super.onPreExecute();
        //do initialization of required objects objects here 
		Log.d("Hadian", "Running Get All History Service");
		progressDialog = new ProgressDialog(this.context);
		progressDialog.setTitle("Inrewards"); //title
		progressDialog.setMessage("Requesting.."); // message
		progressDialog.setCancelable(false);
		progressDialog.show();
		
    };

	@Override
	protected String doInBackground(Void... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(
				"http://rewards.wiredin.my/services/getRedeemHistory");
		String text = null;
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("userid", dbHandler.getUser().getUserid()));
//	        nameValuePairs.add(new BasicNameValuePair("qr", this.qr));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
			HttpResponse response = httpClient.execute(httppost, localContext);

			HttpEntity entity = response.getEntity();

			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			return e.getLocalizedMessage();
		}

		return text;
	}

	protected void onPostExecute(String results) {
		if (results != null) {
			
			Log.d("Hadian", "Jadi dgn result: "+results);
			progressDialog.dismiss();
			
			DatabaseHandler dbHandler = new DatabaseHandler(context, null, null, 2);
			dbHandler.deleteAllHistory();

			// getfeaturedsales
			JSONObject[] objects = null;
			try {
				String data = results;
				JSONArray array = new JSONArray(data);
				objects = new JSONObject[array.length()];
				History[] history = new History[array.length()];

				for (int i = 0; i < array.length(); i++) {
					objects[i] = array.getJSONObject(i);
					// Log.d("Hadian", "Data: "+array.getJSONObject(i).toString());
					history[i] = new History(
							objects[i].getString("id"),
							objects[i].getString("name"),
							objects[i].getString("picture"),
							objects[i].getString("timestamp"));
				
					dbHandler.addHistory(history[i]);
				}

				context.startActivity(new Intent(context, HistoryActivity.class));
				progressDialog.dismiss();
				
			} catch (Exception e) {
				Log.d("Hadian", "Error: " + e);
				showError();
			}
			
		}
	}
	
	public void showError() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		//black
		alertDialogBuilder.setInverseBackgroundForced(true);
				
		// set title
		alertDialogBuilder.setTitle("InRewards");

		// set dialog message
		alertDialogBuilder
				.setMessage("Service Error")
				.setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
