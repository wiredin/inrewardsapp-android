package con.wiredin.inrewards.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.wiredin.inrewards.UserProfileActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.User;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public class GetUserViewService extends AsyncTask<Void, Void, String> {
	ProgressDialog progressDialog;
	Context context;
	String id;
	String userId;
	
	public GetUserViewService(Context context,String id,String userId){
		this.context = context;
		this.id = id;
		this.userId = userId;
	}
	
	protected String getASCIIContentFromEntity(HttpEntity entity)
			throws IllegalStateException, IOException {
		InputStream in = entity.getContent();

		StringBuffer out = new StringBuffer();
		int n = 1;
		while (n > 0) {
			byte[] b = new byte[4096];
			n = in.read(b);

			if (n > 0)
				out.append(new String(b, 0, n));
		}

		return out.toString();
	}
	
	@Override
    protected void onPreExecute()
    {
        
		super.onPreExecute();
        //do initialization of required objects objects here 

    };

	@Override
	protected String doInBackground(Void... params) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httppost = new HttpPost(
				"http://rewards.wiredin.my/services/getuserview");
		String text = null;
		DatabaseHandler dbHandler = new DatabaseHandler(context, null, null, 2);
		User user = dbHandler.getUser();
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("userid", user.getUserid()));
	        nameValuePairs.add(new BasicNameValuePair("userviewid", this.id));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
			HttpResponse response = httpClient.execute(httppost, localContext);
			HttpEntity entity = response.getEntity();
			text = getASCIIContentFromEntity(entity);

		} catch (Exception e) {
			
			return e.getLocalizedMessage();
			
		}

		return text;
	}

	protected void onPostExecute(String results) {
		if (results != null) {
			
			Log.d("Hadian", "Jadi dgn result: "+results);
			
			DatabaseHandler dbHandler = new DatabaseHandler(context, null, null, 2);
			dbHandler.deleteUserView();

			// getfeaturedsales
			JSONObject[] objects = null;
			try {
				String data = results;
				JSONArray array = new JSONArray(data);
				objects = new JSONObject[array.length()];
				User[] user = new User[array.length()];

				for (int i = 0; i < array.length(); i++) {
					objects[i] = array.getJSONObject(i);
					// Log.d("Hadian", "Data: "+array.getJSONObject(i).toString());
					user[i] = new User(objects[i].getString("login_id"),
							objects[i].getString("fullname"), objects[i].getString("address"),
							objects[i].getString("phone"),objects[i].getString("point_collected"),
							objects[i].getString("ranking"),objects[i].getString("highFive"));
					
					user[i].setHigh_Status(objects[i].getString("high5_status"));
					
					dbHandler.addUserView(user[i]);
					
					
					Intent intent = new Intent(context, UserProfileActivity.class);
					context.startActivity(intent);
				}
				
			} catch (Exception e) {
				Log.d("Hadian", "Error: " + e);
				showError();
			}
			
		}
	}
	
	public void showError() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		//black
		alertDialogBuilder.setInverseBackgroundForced(true);
				
		// set title
		alertDialogBuilder.setTitle("InRewards");

		// set dialog message
		alertDialogBuilder
				.setMessage("Something Wrong")
				.setCancelable(true)
				.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
