package com.wiredin.inrewards;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.wiredin.inrewards.fragment.AnimationFragment;
import com.wiredin.inrewards.fragment.DefaultFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

public class MainActivityBackup extends SherlockFragmentActivity implements
		ActionBar.TabListener {

	private ViewPager mPager;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		PagerAdapter adapter = new FragmentStatePagerAdapter(
				getSupportFragmentManager()) {
			@Override
			public Fragment getItem(int position) {
				switch (position) {
				case 0:
					return new DefaultFragment();
				case 1:
					return new AnimationFragment();
				}
				return null;
			}

			@Override
			public int getCount() {
				return 2;
			}

			@Override
			public CharSequence getPageTitle(int position) {
				switch (position) {
				case 0:
					return getString(R.string.default_fragment);
				case 1:
					return getString(R.string.animation_fragment);
				}
				return null;
			}
		};

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(adapter);
		mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				getSupportActionBar().setSelectedNavigationItem(position);
			}
		});

		mPager.setPageMargin(getResources().getDimensionPixelSize(
				R.dimen.page_margin));

		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		for (int position = 0; position < adapter.getCount(); position++) {
			getSupportActionBar().addTab(
					getSupportActionBar().newTab()
							.setText(adapter.getPageTitle(position))
							.setTabListener(this));
		}

		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public void onTabSelected(Tab tab,
			android.support.v4.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		mPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab,
			android.support.v4.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab,
			android.support.v4.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

}
