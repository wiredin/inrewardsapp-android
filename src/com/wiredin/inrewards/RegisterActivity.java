package com.wiredin.inrewards;

import com.actionbarsherlock.app.SherlockActivity;

import con.wiredin.inrewards.service.RegisterService;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends SherlockActivity {
	
	EditText email;
	EditText password;
	EditText fullname;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		email = (EditText) findViewById(R.id.reg_email);
		password = (EditText) findViewById(R.id.reg_password);
		fullname = (EditText) findViewById(R.id.reg_fullname);
		
		Button btnReg = (Button) findViewById(R.id.btn_reg);
		
		btnReg.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				RegisterService service = new RegisterService(email.getText().toString(),
                		password.getText().toString(),fullname.getText().toString(),
                		RegisterActivity.this);
                service.execute();
            }
		});
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		// TODO Auto-generated method stub
		finish();
		return super.onOptionsItemSelected(item);
	}

}
