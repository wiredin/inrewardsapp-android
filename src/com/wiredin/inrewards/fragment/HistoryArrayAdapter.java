package com.wiredin.inrewards.fragment;

import com.squareup.picasso.Picasso;
import com.wiredin.inrewards.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class HistoryArrayAdapter extends ArrayAdapter<String> {

	private final Activity context;
	private final String[] names;
	private final String[] picture;
	
	int height;

	public HistoryArrayAdapter(Activity context, String[] names, String[] picture) {
		super(context, R.layout.item_list_view, names);
		this.context = context;
		this.names = names;
		this.picture = picture;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.history_list_view, null);
		}

		TextView secondLine = (TextView) rowView.findViewById(R.id.secondLine);
		secondLine.setText(names[position]);
		//secondLine.setVisibility(View.GONE);
		
		ImageView image = (ImageView) rowView.findViewById(R.id.icon);
		
		Picasso.with(context).load("http://rewards.wiredin.my/uploads/"+picture[position])
		.resize(120, 120).centerCrop().into(image);

		return rowView;
	}

}