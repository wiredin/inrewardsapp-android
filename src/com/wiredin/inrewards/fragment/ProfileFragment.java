package com.wiredin.inrewards.fragment;


import com.wiredin.inrewards.R;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.User;

import con.wiredin.inrewards.service.GetHistoryService;
import con.wiredin.inrewards.service.GetLeaderboardServiceMonth;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileFragment extends Fragment {
	Button btnHist;
	Button btnLeader;
	int checkHigh = 0;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.activity_profile_fragment, container,
				false);
		
		DatabaseHandler dbHandler = new DatabaseHandler(getActivity(), null, null, 2);
		User user = dbHandler.getUser();
		
		String fontPath = "fonts/RobotoCondensed-Bold.ttf";
		String fontPathLight = "fonts/RobotoCondensed-Light.ttf";
		String fontPathRegular = "fonts/RobotoCondensed-Light.ttf";
		
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
		Typeface tf2 = Typeface.createFromAsset(getActivity().getAssets(), fontPathLight);
		Typeface tf3 = Typeface.createFromAsset(getActivity().getAssets(), fontPathRegular);
		
		TextView name = (TextView) rootView.findViewById(R.id.profileName);
		//TextView address = (TextView) rootView.findViewById(R.id.profileAddress);
		TextView phone = (TextView) rootView.findViewById(R.id.profilePhone);
		TextView point = (TextView) rootView.findViewById(R.id.profilePoint);
		
		TextView high5 = (TextView) rootView.findViewById(R.id.high5);
		TextView text_high5 = (TextView) rootView.findViewById(R.id.text_high5); 
		TextView rank = (TextView) rootView.findViewById(R.id.rank); 
		TextView text_rank= (TextView) rootView.findViewById(R.id.text_rank);
		TextView text_point = (TextView) rootView.findViewById(R.id.point);
		
		name.setTypeface(tf);
		phone.setTypeface(tf); 
		high5.setTypeface(tf3);
		text_high5.setTypeface(tf);
		rank.setTypeface(tf3);
		text_rank.setTypeface(tf);
		text_point.setTypeface(tf3);
		point.setTypeface(tf);
		
		ImageView shareRank = (ImageView) rootView.findViewById(R.id.rank_share);
		final ImageView highFive = (ImageView) rootView.findViewById(R.id.ic_high5);
		highFive.setVisibility(View.INVISIBLE);
		
		name.setText(user.getFullname());
		//address.setText(user.getAddress());
		phone.setText(user.getPhone());
		point.setText(user.getCurrent_point());
		text_rank.setText(user.getRank());
		text_high5.setText(user.getHigh_Five());
		
		btnHist = (Button) rootView.findViewById(R.id.btnHistory);
		btnHist.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				GetHistoryService service = new GetHistoryService(getActivity());
				service.execute();
			}
		});
		
		btnLeader = (Button) rootView.findViewById(R.id.btnLeader);
		btnLeader.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				GetLeaderboardServiceMonth service = new GetLeaderboardServiceMonth(getActivity());
				service.execute();
			}
		});
		
		shareRank.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent share = new Intent(android.content.Intent.ACTION_SEND);
			    share.setType("text/plain");
			    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			    
			    startActivity(Intent.createChooser(share, "Share Rank!"));
			}
			
		});
		
		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

}
