package com.wiredin.inrewards.fragment;

import com.wiredin.inrewards.LoginActivity;
import com.wiredin.inrewards.MainLayoutActivity;
import com.wiredin.inrewards.R;
import com.wiredin.inrewards.ScanActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.User;
import com.wiredin.inrewards.fragment.CameraPreview;

import con.wiredin.inrewards.service.ScanService;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import net.sourceforge.zbar.Config;

public class ScanFragment extends Fragment {
	
	Button openScanner;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.open_scanner_fragment, container,
				false);
		
		openScanner = (Button) rootView.findViewById(R.id.open_scanner_btn);
		openScanner.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), ScanActivity.class);
				startActivity(intent);
			}
		});
		
		return rootView;
	}

}
