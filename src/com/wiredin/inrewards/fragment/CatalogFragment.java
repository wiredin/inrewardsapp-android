package com.wiredin.inrewards.fragment;

import java.util.List;

import com.wiredin.inrewards.R;
import com.wiredin.inrewards.RedeemActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.Item;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class CatalogFragment extends Fragment {
	
	private ListView itemList1;
	DatabaseHandler dbHandler;
	List<Item> items;
	String[] itemId;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.activity_home_fragment, container,
				false);

		this.itemList1 = (ListView) rootView.findViewById(R.id.list1);
		
		dbHandler = new DatabaseHandler(getActivity(), null, null, 2);
		items = dbHandler.getAllItem();

		if (items != null) {
			String[] itemName = new String[items.size()];
			String[] itemPicture = new String[items.size()];
			String[] itemPoint = new String[items.size()];
			itemId = new String[items.size()];

			int counter = 0;
			for (Item item : items) {
				itemName[counter] = item.getName();
				itemPicture[counter] = item.getPicture();
				itemPoint[counter] = item.getPoint_redeem();
				itemId[counter] = item.getItem_id();
				counter++;
			}

			ItemArrayAdapter adapter = new ItemArrayAdapter(getActivity(),
					itemName, itemPoint, itemPicture);
			itemList1.setAdapter(adapter);
		}

		itemList1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {

				Intent i = new Intent(getActivity(),
						RedeemActivity.class);
				i.putExtra("itemid", itemId[position]);
				Log.d("Hadian", "Item : " + position);
				startActivity(i);

			}

		});
		
		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

}
