package com.wiredin.inrewards.fragment;

import com.wiredin.inrewards.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class LeaderListArrayAdapter extends ArrayAdapter<String> {

	private final Activity context;
	private final String[] names;
	private final String[] point;
	private final String[] viewedId;
	
	int height;

	public LeaderListArrayAdapter(Activity context, String[] names,String[] point, String[] viewedId) {
		super(context, R.layout.item_list_view, names);
		this.context = context;
		this.names = names;
		this.point = point;
		this.viewedId = viewedId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.item_list_view, null);
		}
		
		ImageView image = (ImageView) rowView.findViewById(R.id.icon);
		image.setVisibility(View.GONE);
		TextView firstLine = (TextView) rowView.findViewById(R.id.firstLine);
		firstLine.setText(names[position]);
		TextView secondLine = (TextView) rowView.findViewById(R.id.secondLine);
		secondLine.setText(point[position]);
		TextView pointText = (TextView) rowView.findViewById(R.id.pointtext);
		//pointText.setText(viewedId[position]);
		pointText.setVisibility(View.GONE);
		
		return rowView;
	}

}