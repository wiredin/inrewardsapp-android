package com.wiredin.inrewards;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.fragment.CatalogFragment;
import com.wiredin.inrewards.fragment.HomeFragment;
import com.wiredin.inrewards.fragment.ProfileFragment;
import com.wiredin.inrewards.fragment.ScanFragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

public class MainActivity extends SherlockFragmentActivity implements
		ActionBar.TabListener {

	private ViewPager mPager;
	DatabaseHandler dbHandler = new DatabaseHandler(this, null, null, 1);

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		PagerAdapter adapter = new FragmentStatePagerAdapter(
				getSupportFragmentManager()) {
			@Override
			public Fragment getItem(int position) {
				switch (position) {
				case 0:
					return new HomeFragment();
				case 1:
					return new CatalogFragment();
				case 2:
					return new ScanFragment();
				case 3:
					return new ProfileFragment();
				}
				return null;
			}

			@Override
			public int getCount() {
				return 4;
			}

		};

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(adapter);
		mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				getSupportActionBar().setSelectedNavigationItem(position);
			}
		});

		mPager.setPageMargin(getResources().getDimensionPixelSize(
				R.dimen.page_margin));

		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		for (int position = 0; position < adapter.getCount(); position++) {
			getSupportActionBar().addTab(
					getSupportActionBar().newTab()
							.setIcon(this.getPageIcon(position))
							.setTabListener(this));
		}

		// getSupportActionBar().setDisplayShowHomeEnabled(false);
		// getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	public Drawable getPageIcon(int position) {
		switch (position) {
		case 0:
			return getResources().getDrawable(R.drawable.first_selected);
		case 1:
			return getResources().getDrawable(R.drawable.ic_action_view_as_list);
		case 2:
			return getResources().getDrawable(R.drawable.second_selected);
		case 3:
			return getResources().getDrawable(R.drawable.third_selected);
		}
		return null;
	}

	@Override
	public void onTabSelected(Tab tab,
			android.support.v4.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		mPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab,
			android.support.v4.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab,
			android.support.v4.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.profile, menu);
		
//		menu.add("Setting")
//		.setIcon(R.drawable.ic_action_overflow)
//		.setShowAsAction(
//				MenuItem.SHOW_AS_ACTION_IF_ROOM
//						| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.profile:

//			Toast.makeText(MainActivity.this, "function 1 called",
//					Toast.LENGTH_SHORT).show();
			this.startActivity(new Intent(this, EditProfileActivity.class));
			break;

		case R.id.logout:

			dbHandler.deleteUser();
			this.startActivity(new Intent(this, MainLayoutActivity.class));
			this.finish();
			break;

		case R.id.cancel:

			closeOptionsMenu();
			break;
		}
		
//		String menu = item.getTitle().toString();
//		
//		if (menu.equals("Setting")) {
//			openOptionsMenu();
//			Toast.makeText(MainActivity.this, "Jadi jadi jadi",
//					Toast.LENGTH_SHORT).show();
//		}

		return true;
	}

}
