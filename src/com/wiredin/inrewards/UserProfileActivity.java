package com.wiredin.inrewards;

import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.LeaderBoard;
import com.wiredin.inrewards.database.User;

import con.wiredin.inrewards.service.HighFiveService;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class UserProfileActivity extends Activity {

	TextView profileName, high5, text_high5, rank, text_rank, point,
			text_point;
	String value;
	ImageView highFive;
	String h5_status;
	int checkHigh = 0;
	User viewedUser, user;
	int totalH5;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			value = extras.getString("viewedId");
			Log.d("Hadian", "view user id: " + value);
		}
		
		final Context context = getApplicationContext();
		
		DatabaseHandler dbHandler = new DatabaseHandler(this, null, null, 2);
		viewedUser = dbHandler.getUserView();
		user = dbHandler.getUser();
		

		profileName = (TextView) findViewById(R.id.profile_name);
		high5 = (TextView) findViewById(R.id.high5);
		text_high5 = (TextView) findViewById(R.id.text_high5);
		rank = (TextView) findViewById(R.id.rank);
		text_rank = (TextView) findViewById(R.id.text_rank);
		point = (TextView) findViewById(R.id.point);
		text_point = (TextView) findViewById(R.id.text_point);

		String fontPath = "fonts/RobotoCondensed-Bold.ttf";
		String fontPathLight = "fonts/RobotoCondensed-Light.ttf";
		String fontPathRegular = "fonts/RobotoCondensed-Light.ttf";

		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPathLight);
		Typeface tf3 = Typeface.createFromAsset(getAssets(), fontPathRegular);

		// Applying font
		profileName.setTypeface(tf);
		high5.setTypeface(tf2);
		text_high5.setTypeface(tf);
		rank.setTypeface(tf2);
		text_rank.setTypeface(tf);
		point.setTypeface(tf2);
		text_point.setTypeface(tf);
		
		profileName.setText(viewedUser.getFullname());
		text_point.setText(viewedUser.getCurrent_point());
		text_high5.setText(viewedUser.getHigh_Five());
		text_rank.setText(viewedUser.getRank());

		highFive = (ImageView) findViewById(R.id.imageView4);
		Log.d("fared ",user.getUserid() +" "+viewedUser.getUserid());
		
		if(user.getUserid().equals(viewedUser.getUserid())){
			highFive.setVisibility(View.GONE);
		}
		
		h5_status = viewedUser.getHigh_Status();
		Log.d("hadian", "high five status "+h5_status);
		
		if(h5_status.equals("null")){
			highFive.setImageResource(R.drawable.ic_high);
			Log.d("fared", "h5 is null");
		} else {
			highFive.setImageResource(R.drawable.ic_highed);
			Log.d("fared", "h5 is available");
		}
		
		totalH5 = Integer.parseInt(viewedUser.getHigh_Five());

		highFive.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (h5_status.equals("null")) {
					Log.d("fared", "click h5 null");
					highFive.setImageResource(R.drawable.ic_highed);
					HighFiveService service = new HighFiveService(UserProfileActivity.this,user.getUserid(),viewedUser.getUserid());
					service.execute();
					totalH5 = totalH5+1;
					text_high5.setText(String.valueOf(totalH5));
				} else {
					Toast.makeText(context, "You already High five this user", Toast.LENGTH_SHORT).show();;
				}

			}
		});
	}

}
