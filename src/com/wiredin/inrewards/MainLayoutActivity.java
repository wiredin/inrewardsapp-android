package com.wiredin.inrewards;

import com.actionbarsherlock.app.SherlockActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.User;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainLayoutActivity extends SherlockActivity {

	Button signup;
	Button login;
	DatabaseHandler dbHandler;
	User user;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_layout);
		
		dbHandler = new DatabaseHandler(this, null, null, 1);
		
		getSupportActionBar().hide();
		
		login = (Button) findViewById(R.id.btn_login);
		signup = (Button) findViewById(R.id.btn_signup);
		
		login.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				user = dbHandler.getUser();
				
				if (user == null) {
					MainLayoutActivity.this.startActivity(new Intent(MainLayoutActivity.this,LoginActivity.class));
				} else {
					MainLayoutActivity.this.startActivity(new Intent(MainLayoutActivity.this,MainActivity.class));
				}
				
			}
		});
		
		signup.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainLayoutActivity.this.startActivity(new Intent(MainLayoutActivity.this,RegisterActivity.class));
			}
		});
		
	}
	
	

}
