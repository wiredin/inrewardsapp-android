package com.wiredin.inrewards;

import java.util.List;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.SherlockListActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.LeaderBoard;
import com.wiredin.inrewards.fragment.LeaderListArrayAdapter;
import com.wiredin.inrewards.listFragment.MonthlyFragment;
import com.wiredin.inrewards.listFragment.WeeklyFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class LeaderListActivity extends SherlockFragmentActivity {

	private ListView itemList1;
	DatabaseHandler dbHandler;

	Button weekBtn;
	Button monthBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_leader_list);
		
		this.getSupportActionBar().setTitle("Leaderboard");

		weekBtn = (Button) findViewById(R.id.week_btn);

		monthBtn = (Button) findViewById(R.id.month_btn);
		
		weekBtn.setSelected(true);

		weekBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				weekBtn.setSelected(true);
				monthBtn.setSelected(false);
				
				Fragment newFragment = new WeeklyFragment();
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.list_container, newFragment);

				// Commit the transaction
				transaction.commit();

			}
		});

		monthBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				monthBtn.setSelected(true);
				weekBtn.setSelected(false);
				
				Fragment newFragment = new MonthlyFragment();
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

				// Replace whatever is in the fragment_container view with this fragment,
				// and add the transaction to the back stack
				transaction.replace(R.id.list_container, newFragment);

				// Commit the transaction
				transaction.commit();
			}
		});

		// this.getSupportActionBar().setTitle("Leaderboard");
		//
		// dbHandler = new DatabaseHandler(this, null, null, 2);
		// List<LeaderBoard> leaders = dbHandler.getLeaderboard();
		//
		// itemList1 = (ListView) findViewById(android.R.id.list);
		//
		// if (leaders != null) {
		// String[] itemName = new String[leaders.size()];
		// String[] itemPoint = new String[leaders.size()];
		//
		// int counter = 0;
		// for (LeaderBoard leader : leaders) {
		// itemName[counter] = leader.getFullname();
		// itemPoint[counter] = leader.getCurrent_point();
		// counter++;
		// }
		//
		// LeaderListArrayAdapter adapter = new LeaderListArrayAdapter(this,
		// itemName,itemPoint);
		// itemList1.setAdapter(adapter);
		// }

	}

	@Override
    protected void onResume() {
        super.onResume();

        if (getSupportFragmentManager().findFragmentById(R.id.list_container) == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.list_container, new WeeklyFragment()).commit();

        }
    }
	
}
