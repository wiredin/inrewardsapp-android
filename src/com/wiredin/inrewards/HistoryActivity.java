package com.wiredin.inrewards;

import java.util.List;

import com.actionbarsherlock.app.SherlockListActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.History;
import com.wiredin.inrewards.fragment.HistoryArrayAdapter;

import android.os.Bundle;
import android.widget.ListView;

public class HistoryActivity extends SherlockListActivity {

	private ListView itemList1;
	DatabaseHandler dbHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_hostory);
		
		this.getSupportActionBar().setTitle("Redeem History");
		
		dbHandler = new DatabaseHandler(this, null, null, 2);
		List<History> histories = dbHandler.getHistory();
		
		itemList1 = (ListView) findViewById(android.R.id.list);
		
		if (histories != null) {
			String[] itemName = new String[histories.size()];
			String[] itemPicture = new String[histories.size()];
			
			int counter = 0;
			for (History history : histories) {
				itemName[counter] = history.getName();
				itemPicture[counter] = history.getPicture();
				counter++;
			}
//			
//			History last = histories.get(histories.size()-1);
			
			HistoryArrayAdapter adapter = new HistoryArrayAdapter(this, itemName, itemPicture);
			itemList1.setAdapter(adapter);
		}

	}

}
