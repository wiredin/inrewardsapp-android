package com.wiredin.inrewards;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.squareup.picasso.Picasso;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.Item;
import com.wiredin.inrewards.database.User;

public class RedeemReceiptActivity extends SherlockActivity {

	String value;
	String timeStamp;
	DatabaseHandler dbHandler;
	Item item;
	User user;
	TextView textName, textItempoint, textBalance, date;
	ImageView itemPic;

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_receipt);
		
		this.getSupportActionBar().setTitle("Redeem Receipt");

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			value = extras.getString("itemid");
			timeStamp = extras.getString("time");
			Log.d("Hadian", "value recieve: " + value);
		}

		dbHandler = new DatabaseHandler(this, null, null, 1);
		item = dbHandler.getItemById(value);
		user = dbHandler.getUser();

		textName = (TextView) findViewById(R.id.text_name);
		textItempoint = (TextView) findViewById(R.id.text_itempoint);
		textBalance = (TextView) findViewById(R.id.text_balance);
		date = (TextView) findViewById(R.id.text_date);

		itemPic = (ImageView) findViewById(R.id.image);

		Picasso.with(this)
				.load("http://rewards.wiredin.my/uploads/" + item.getPicture())
				.resize(360, 360).centerCrop().into(itemPic);
		
		textName.setText(item.getName());
		textItempoint.setText(item.getPoint_redeem());
		textBalance.setText(user.getCurrent_point());
		date.setText(timeStamp);

		Button shareBtn = (Button) findViewById(R.id.btn_share);
		shareBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent share = new Intent(android.content.Intent.ACTION_SEND);
				share.setType("text/plain");
				share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

				startActivity(Intent.createChooser(share,
						"Share Your Redeem to :"));
			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

}
