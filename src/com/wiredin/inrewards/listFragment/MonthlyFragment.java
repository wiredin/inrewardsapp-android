package com.wiredin.inrewards.listFragment;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockListFragment;
import com.wiredin.inrewards.R;
import com.wiredin.inrewards.UserProfileActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.LeaderBoard;
import com.wiredin.inrewards.fragment.LeaderListArrayAdapter;

import con.wiredin.inrewards.service.GetUserViewService;

public class MonthlyFragment extends SherlockFragment {

	String value;
	ListView itemList1;
	DatabaseHandler dbHandler;
	String[] viewedId;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.activity_month_leader,
				container, false);
		
		Log.d("fared", "fragment monthly");

		dbHandler = new DatabaseHandler(this.getActivity(), null, null, 2);
		List<LeaderBoard> leaders = dbHandler.getLeaderboardMonth();

		itemList1 = (ListView) rootView.findViewById(R.id.list2);

		if (leaders != null) {
			String[] itemName = new String[leaders.size()];
			String[] itemPoint = new String[leaders.size()];
			viewedId = new String[leaders.size()];

			int counter = 0;
			for (LeaderBoard leader : leaders) {
				viewedId[counter] = leader.getLogin_id();
				itemName[counter] = leader.getFullname();
				itemPoint[counter] = leader.getCurrent_point();
				counter++;
			}

			LeaderListArrayAdapter adapter = new LeaderListArrayAdapter(
					this.getActivity(), itemName, itemPoint, viewedId);
			itemList1.setAdapter(adapter);
		}

		itemList1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				Log.d("Hadian", "Item : " + viewedId[position]);
				GetUserViewService service = new GetUserViewService(getActivity(), viewedId[position], dbHandler.getUser().getUserid());
				service.execute();
				
//				Intent i = new Intent(getActivity(), UserProfileActivity.class);
//				i.putExtra("viewedId", viewedId[position]);
//				Log.d("Hadian", "Item : " + position);
//				startActivity(i);

			}

		});

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

}
