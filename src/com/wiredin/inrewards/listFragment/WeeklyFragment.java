package com.wiredin.inrewards.listFragment;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.wiredin.inrewards.R;
import com.wiredin.inrewards.UserProfileActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.LeaderBoard;
import com.wiredin.inrewards.fragment.LeaderListArrayAdapter;

public class WeeklyFragment extends SherlockListFragment {

	private ListView itemList1;
	DatabaseHandler dbHandler;

	String[] viewedId;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View rootView = inflater.inflate(R.layout.activity_weekly_leader,
				container, false);

		Log.d("fared", "fragment weekly");
		
		dbHandler = new DatabaseHandler(this.getActivity(), null, null, 2);
		List<LeaderBoard> leaders = dbHandler.getLeaderboardWeek();

		itemList1 = (ListView) rootView.findViewById(android.R.id.list);

		if (leaders != null) {
			String[] itemName = new String[leaders.size()];
			String[] itemPoint = new String[leaders.size()];
			viewedId = new String[leaders.size()];

			int counter = 0;
			for (LeaderBoard leader : leaders) {
				itemName[counter] = leader.getFullname();
				itemPoint[counter] = leader.getCurrent_point();
				viewedId[counter] = leader.getLogin_id();
				counter++;
			}

			LeaderListArrayAdapter adapter = new LeaderListArrayAdapter(
					this.getActivity(), itemName, itemPoint,viewedId);
			itemList1.setAdapter(adapter);
		}

		itemList1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {

//				Intent i = new Intent(getActivity(), UserProfileActivity.class);
				// i.putExtra("itemid", itemId[position]);
				Log.d("Hadian", "Item : " + position);
//				startActivity(i);

			}

		});

		return rootView;
	}

}
