package com.wiredin.inrewards;

import java.util.List;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.squareup.picasso.Picasso;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.Item;
import com.wiredin.inrewards.database.User;
import com.wiredin.inrewards.fragment.ItemArrayAdapter;

import con.wiredin.inrewards.service.RedeemService;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RedeemActivity extends SherlockFragmentActivity {

	String value;
	static Item item;
	User user;
	View detailFocus, relatedFocus;
	RelativeLayout btnDetails, btnRelated;
	DatabaseHandler dbHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			value = extras.getString("itemid");
			Log.d("Hadian", "value recieve: " + value);
		}

		// this.getSupportActionBar().hide();

		setContentView(R.layout.activity_redeem_backup);

		dbHandler = new DatabaseHandler(this, null, null, 1);
		item = dbHandler.getItemById(value);
		user = dbHandler.getUser();

		TextView name = (TextView) findViewById(R.id.redeemName);
		TextView point = (TextView) findViewById(R.id.redeemPoint);
		TextView expired = (TextView) findViewById(R.id.redeemExpired);

		ImageView picture = (ImageView) findViewById(R.id.redeemPicture);

		detailFocus = (View) findViewById(R.id.focus_details);
		relatedFocus = (View) findViewById(R.id.focus_related);

		detailFocus.setVisibility(View.VISIBLE);

		btnDetails = (RelativeLayout) findViewById(R.id.detail_btn);
		btnRelated = (RelativeLayout) findViewById(R.id.related_btn);

		btnDetails.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				detailFocus.setVisibility(View.VISIBLE);
				relatedFocus.setVisibility(View.INVISIBLE);

				if (getSupportFragmentManager().findFragmentById(
						R.id.redeem_container) != null) {

					Fragment newFragment = new DetailsFragment();
					FragmentTransaction transaction = getSupportFragmentManager()
							.beginTransaction();

					// Replace whatever is in the fragment_container view with
					// this fragment,
					// and add the transaction to the back stack
					transaction.replace(R.id.redeem_container, newFragment);

					// Commit the transaction
					transaction.commit();
				}
			}
		});

		btnRelated.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				relatedFocus.setVisibility(View.VISIBLE);
				detailFocus.setVisibility(View.INVISIBLE);

				if (getSupportFragmentManager().findFragmentById(
						R.id.redeem_container) != null) {
					Fragment newFragment = new RelatedFragment();
					FragmentTransaction transaction = getSupportFragmentManager()
							.beginTransaction();

					// Replace whatever is in the fragment_container view with
					// this fragment,
					// and add the transaction to the back stack
					transaction.replace(R.id.redeem_container, newFragment);

					// Commit the transaction
					transaction.commit();
				}
			}
		});

		Picasso.with(this)
				.load("http://rewards.wiredin.my/uploads/" + item.getPicture())
				.resize(360, 360).centerCrop().into(picture);

		// picture.setImageBitmap(roundCornerImage(BitmapFactory.decodeResource(getResources(),
		// R.drawable.bg_header),60));

		name.setText(item.getName());
		point.setText(item.getPoint_redeem());
		expired.setText(item.getExpired_date());

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		Button btnRedeem = (Button) findViewById(R.id.redeemBtn);

		btnRedeem.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						RedeemActivity.this);

				// set title
				alertDialogBuilder.setTitle("Your Title");

				// set dialog message
				alertDialogBuilder
						.setMessage("Are you sure want to redeem this item ?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, close
										// current activity

										RedeemService service = new RedeemService(
												user.getUserid(), item
														.getItem_id(),
												RedeemActivity.this);
										service.execute();

									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, just close
										// the dialog box and do nothing

										dialog.dismiss();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(
			com.actionbarsherlock.view.MenuItem item) {
		// TODO Auto-generated method stub
		finish();
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (getSupportFragmentManager().findFragmentById(R.id.redeem_container) == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.redeem_container, new DetailsFragment()).commit();
		}
	}

	public static class DetailsFragment extends SherlockFragment {

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			View rootView = inflater.inflate(R.layout.fragment_details, null);

			TextView detail = (TextView) rootView
					.findViewById(R.id.redeemDetail);

			detail.setText(item.getDetail());

			return rootView;
		}

	}

	public static class RelatedFragment extends SherlockFragment {

		static List<Item> items;
		String[] itemId;
		DatabaseHandler dbHandler;

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			View rootView = inflater.inflate(R.layout.fragment_related, null);

			ListView listView;

			//String[] from = { "php_key", "c_key", "android_key", "hacking_key" };

			listView = (ListView) rootView.findViewById(R.id.list);

			dbHandler = new DatabaseHandler(getActivity(), null, null, 2);
			items = dbHandler.getAllItem();

			if (items != null) {
				String[] itemName = new String[items.size()];
				String[] itemPicture = new String[items.size()];
				String[] itemPoint = new String[items.size()];
				itemId = new String[items.size()];

				int counter = 0;
				for (Item item : items) {
					itemName[counter] = item.getName();
					itemPicture[counter] = item.getPicture();
					itemPoint[counter] = item.getPoint_redeem();
					itemId[counter] = item.getItem_id();
					counter++;
				}

				ItemArrayAdapter adapter = new ItemArrayAdapter(getActivity(),
						itemName, itemPoint, itemPicture);
				listView.setAdapter(adapter);
			}

			listView
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								final View view, int position, long id) {

							Intent i = new Intent(getActivity(),
									RedeemActivity.class);
							i.putExtra("itemid", itemId[position]);
							Log.d("Hadian", "Item : " + position);
							startActivity(i);
							getActivity().finish();

						}

					});

//			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
//					getActivity(), R.layout.related_list_view, R.id.redeemName,
//					from);
//
//			listView.setAdapter(adapter);

			return rootView;
		}

	}

}
