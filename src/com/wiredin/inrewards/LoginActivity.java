package com.wiredin.inrewards;

import com.actionbarsherlock.app.SherlockActivity;

import con.wiredin.inrewards.service.LoginService;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends SherlockActivity {
	
	EditText email;
	EditText password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		email = (EditText) findViewById(R.id.email);
		password = (EditText) findViewById(R.id.password);
		
		Button btnLogin = (Button) findViewById(R.id.btnLogin);
		
		btnLogin.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				LoginService service = new LoginService(email.getText().toString(),
                		password.getText().toString(),LoginActivity.this);
                service.execute();
            }
		});
	}

}
