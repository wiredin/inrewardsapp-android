package com.wiredin.inrewards;

import com.actionbarsherlock.app.SherlockActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.User;

import android.os.Bundle;
import android.content.Intent;
public class SplashScreen extends SherlockActivity {
	
	DatabaseHandler dbHandler = new DatabaseHandler(this, null, null, 1);
	User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(1000);
				} catch (InterruptedException e){
					e.printStackTrace();
				}finally{
					
					user = dbHandler.getUser();
					
					if (user == null) {
						SplashScreen.this.startActivity(new Intent(SplashScreen.this, MainLayoutActivity.class));
					}else{
						SplashScreen.this.startActivity(new Intent(SplashScreen.this, MainActivity.class));
					}
					
					finish();
					
				}
			}
		};
		timer.start();
	}

}
