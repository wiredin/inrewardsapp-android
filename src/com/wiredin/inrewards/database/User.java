package com.wiredin.inrewards.database;

public class User {
	private int _id;
	private String user_id;
	private String fullname;
	private String address;
	private String phone;
	private String current_point;
	private String rank;
	private String high_five;
	private String status;

	public User() {
		
	}
	
	public User(int id, String user_id, String fullname, String address, 
			String phone, String current_point, String rank, String high_five) {
		this._id = id;
		this.user_id = user_id;
		this.fullname = fullname;
		this.address = address;
		this.phone = phone;
		this.current_point = current_point;
		this.rank = rank;
		this.high_five = high_five;
	}
	
	public User(String user_id, String fullname, String address, String phone, 
			String current_point, String rank, String high_five) {
		this.user_id = user_id;
		this.fullname = fullname;
		this.address = address;
		this.phone = phone;
		this.current_point = current_point;
		this.rank = rank;
		this.high_five = high_five;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setUser_id(String text){
		this.user_id = text;
	}
	public void setFullname(String text){
		this.fullname = text;
	}
	public void setAddress(String text){
		this.address = text;
	}
	public void setPhone(String text){
		this.phone = text;
	}
	public void setCurrent_point(String text){
		this.current_point = text;
	}
	public void setRank(String text){
		this.rank = text;
	}
	public void setHigh_Five(String text){
		this.high_five = text;
	}
	public void setHigh_Status(String text){
		this.status = text;
	}
	
	
	public int getId(){
		return this._id;
	}
	public String getUserid(){
		return this.user_id;
	}
	public String getFullname(){
		return this.fullname;
	}
	public String getAddress(){
		return this.address;
	}
	public String getPhone(){
		return this.phone;
	}
	public String getCurrent_point(){
		return this.current_point;
	}
	public String getRank(){
		return this.rank;
	}
	public String getHigh_Five(){
		return this.high_five;
	}
	public String getHigh_Status(){
		return this.status;
	}

}