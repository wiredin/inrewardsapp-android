package com.wiredin.inrewards.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "inrewards.db";
	private static final String TABLE_USER = "user";
	private static final String TABLE_USERVIEW = "userview";
	private static final String TABLE_HISTORY = "history";
	private static final String TABLE_ITEM = "item";
	private static final String TABLE_LEADER_MONTH = "leaderboardM";
	private static final String TABLE_LEADER_WEEK = "leaderboardW";
	private static final String TABLE_GCM = "GCM";

	public static final String USER_TABLE_ID = "_id";
	public static final String USER_ID = "user_id";
	public static final String USER_NAME = "fullname";
	public static final String USER_ADDRESS = "address";
	public static final String USER_PHONE = "phone";
	public static final String USER_POINT = "current_point";
	public static final String USER_RANK = "rank";
	public static final String USER_HIGHFIVE = "high_five";

	public static final String USERV_TABLE_ID = "_id";
	public static final String USERV_ID = "user_id";
	public static final String USERV_NAME = "fullname";
	public static final String USERV_POINT = "current_point";
	public static final String USERV_RANK = "rank";
	public static final String USERV_HIGHFIVE = "high_five";
	public static final String USERV_H5STATUS = "status";

	public static final String ITEM_TABLE_ID = "_id";
	public static final String ITEM_ID = "item_id";
	public static final String ITEM_NAME = "name";
	public static final String ITEM_DETAIL = "detail";
	public static final String ITEM_POINT = "point_redeem";
	public static final String ITEM_PICTURE = "picture";
	public static final String ITEM_TOTAL = "total";
	public static final String ITEM_EXPIRED = "expired_date";

	public static final String HISTORY_TABLE_ID = "_id";
	public static final String HISTORY_ID = "history_id";
	public static final String HISTORY_NAME = "name";
	public static final String HISTORY_PICTURE = "picture";
	public static final String HISTORY_TIMESTAMP = "timestamp";

	public static final String LEADERM_TABLE_ID = "_id";
	public static final String LEADERM_FULLNAME = "fullname";
	public static final String LEADERM_CURRENT_POINT = "current_point";
	public static final String LEADERM_LOGIN_ID = "login_id";
	public static final String LEADERM_PHONE = "phone";
	public static final String LEADERM_RANK = "rank";
	public static final String LEADERM_HIGHFIVE = "high_five";
	public static final String LEADERM_H5STATUS = "status";

	public static final String LEADERW_TABLE_ID = "_id";
	public static final String LEADERW_FULLNAME = "fullname";
	public static final String LEADERW_CURRENT_POINT = "current_point";
	public static final String LEADERW_LOGIN_ID = "login_id";
	public static final String LEADERW_PHONE = "phone";
	public static final String LEADERW_RANK = "rank";
	public static final String LEADERW_HIGHFIVE = "high_five";
	public static final String LEADERW_H5STATUS = "status";

	public static final String GCM_ID = "_id";
	public static final String GCM_GCMREGID = "gcm_regid";
	public static final String GCM_APPVERSION = "appversion";

	public DatabaseHandler(Context context, String name, CursorFactory factory,
			int version) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_USER + "("
				+ USER_TABLE_ID + " INTEGER PRIMARY KEY," + USER_ID + " TEXT,"
				+ USER_NAME + " TEXT," + USER_ADDRESS + " TEXT," + USER_PHONE
				+ " TEXT," + USER_RANK + " TEXT," + USER_HIGHFIVE + " TEXT,"
				+ USER_POINT + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_USER);
		String CREATE_TABLE_USERVIEW = "CREATE TABLE " + TABLE_USERVIEW + "("
				+ USERV_TABLE_ID + " INTEGER PRIMARY KEY," + USERV_ID
				+ " TEXT," + USERV_NAME + " TEXT," + USERV_H5STATUS + " TEXT,"
				+ USERV_RANK + " TEXT," + USERV_HIGHFIVE + " TEXT,"
				+ USERV_POINT + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_USERVIEW);
		String CREATE_TABLE_HISTORY = "CREATE TABLE " + TABLE_HISTORY + "("
				+ HISTORY_TABLE_ID + " INTEGER PRIMARY KEY," + HISTORY_ID
				+ " TEXT," + HISTORY_NAME + " TEXT," + HISTORY_PICTURE
				+ " TEXT," + HISTORY_TIMESTAMP + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_HISTORY);
		String CREATE_TABLE_ITEM = "CREATE TABLE " + TABLE_ITEM + "("
				+ ITEM_TABLE_ID + " INTEGER PRIMARY KEY," + ITEM_ID + " TEXT,"
				+ ITEM_NAME + " TEXT," + ITEM_DETAIL + " TEXT," + ITEM_POINT
				+ " TEXT," + ITEM_PICTURE + " TEXT, " + ITEM_TOTAL + " TEXT,"
				+ ITEM_EXPIRED + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_ITEM);
		String CREATE_TABLE_LEADERM = "CREATE TABLE " + TABLE_LEADER_MONTH
				+ "(" + LEADERM_TABLE_ID + " INTEGER PRIMARY KEY,"
				+ LEADERM_FULLNAME + " TEXT," + LEADERM_LOGIN_ID + " TEXT,"
				+ LEADERM_CURRENT_POINT + " TEXT," + LEADERM_PHONE + " TEXT,"
				+ LEADERM_RANK + " TEXT," + LEADERM_HIGHFIVE + " TEXT"
				+ LEADERM_H5STATUS + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_LEADERM);
		String CREATE_TABLE_LEADERW = "CREATE TABLE " + TABLE_LEADER_WEEK + "("
				+ LEADERW_TABLE_ID + " INTEGER PRIMARY KEY," + LEADERW_FULLNAME
				+ " TEXT," + LEADERW_LOGIN_ID + " TEXT,"
				+ LEADERW_CURRENT_POINT + " TEXT," + LEADERW_PHONE + " TEXT,"
				+ LEADERW_RANK + " TEXT," + LEADERW_HIGHFIVE + " TEXT"
				+ LEADERW_H5STATUS + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_LEADERW);
		String CREATE_TABLE_GCM = "CREATE TABLE " + TABLE_GCM + "(" + GCM_ID
				+ " INTEGER PRIMARY KEY," + GCM_GCMREGID + " TEXT,"
				+ GCM_APPVERSION + " TEXT" + ")";
		db.execSQL(CREATE_TABLE_GCM);
		Log.v("Hadian", "Database Created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.v("Hadian", "Database Updated form " + oldVersion + " to "
				+ newVersion);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERVIEW);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_GCM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LEADER_MONTH);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LEADER_WEEK);
		onCreate(db);
	}

	public void addUser(User user) {

		ContentValues values = new ContentValues();
		values.put(USER_TABLE_ID, 1);
		values.put(USER_ID, user.getUserid());
		values.put(USER_NAME, user.getFullname());
		values.put(USER_ADDRESS, user.getAddress());
		values.put(USER_PHONE, user.getPhone());
		values.put(USER_POINT, user.getCurrent_point());
		values.put(USER_RANK, user.getRank());
		values.put(USER_HIGHFIVE, user.getHigh_Five());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_USER, null, values);
		db.close();

		Log.v("Hadian", "User Added");
	}

	public void deleteUser() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_USER);
		db.close();
		Log.v("Hadian", "User Deleted");
	}

	public User getUser() {
		// Select All Query
		int id = 1;

		String query = "Select * FROM " + TABLE_USER + " WHERE "
				+ USER_TABLE_ID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		User user = new User();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				user.setId(Integer.parseInt(cursor.getString(0)));
				user.setUser_id(cursor.getString(1));
				user.setFullname(cursor.getString(2));
				user.setAddress(cursor.getString(3));
				user.setPhone(cursor.getString(4));
				user.setRank(cursor.getString(5));
				user.setHigh_Five(cursor.getString(6));
				user.setCurrent_point(cursor.getString(7));

			} while (cursor.moveToNext());

			Log.v("Hadian", "User -" + user.getFullname());
		} else {
			user = null;
		}
		db.close();

		// return contact list
		return user;
	}

	public void addUserView(User user) {

		ContentValues values = new ContentValues();
		values.put(USERV_TABLE_ID, 1);
		values.put(USERV_ID, user.getUserid());
		values.put(USERV_NAME, user.getFullname());
		values.put(USERV_H5STATUS, user.getHigh_Status()); //tuka address pade status H5 sebab address tak gune
		values.put(USERV_POINT, user.getCurrent_point());
		values.put(USERV_RANK, user.getRank());
		values.put(USERV_HIGHFIVE, user.getHigh_Five());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_USERVIEW, null, values);
		db.close();

		Log.v("Hadian", "User Added");
	}

	public void deleteUserView() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_USERVIEW);
		db.close();
		Log.v("Hadian", "User Deleted");
	}

	public User getUserView() {
		// Select All Query
		int id = 1;

		String query = "Select * FROM " + TABLE_USERVIEW + " WHERE "
				+ USERV_TABLE_ID + " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		User user = new User();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				user.setId(Integer.parseInt(cursor.getString(0)));
				user.setUser_id(cursor.getString(1));
				user.setFullname(cursor.getString(2));
				user.setHigh_Status(cursor.getString(3));
				user.setRank(cursor.getString(4));
				user.setHigh_Five(cursor.getString(5));
				user.setCurrent_point(cursor.getString(6));

			} while (cursor.moveToNext());

			Log.v("Hadian", "User -" + user.getFullname());
		} else {
			user = null;
		}
		db.close();

		// return contact list
		return user;
	}

	public void addItem(Item item) {

		ContentValues values = new ContentValues();
		values.put(ITEM_ID, item.getItem_id());
		values.put(ITEM_NAME, item.getName());
		values.put(ITEM_DETAIL, item.getDetail());
		values.put(ITEM_POINT, item.getPoint_redeem());
		values.put(ITEM_PICTURE, item.getPicture());
		values.put(ITEM_TOTAL, item.getTotal());
		values.put(ITEM_EXPIRED, item.getExpired_date());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_ITEM, null, values);
		db.close();

		Log.v("Hadian", "Item Added - " + item.getName());
	}

	public void deleteAllItem() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_ITEM);
		db.close();
		Log.v("Hadian", "All Item Deleted");
	}

	public Item getItemById(String id) {
		// Select All Query

		String query = "Select * FROM " + TABLE_ITEM + " WHERE " + ITEM_ID
				+ " =  \"" + id + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		Item item = new Item();

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				item.setId(Integer.parseInt(cursor.getString(0)));
				item.setItem_id(cursor.getString(1));
				item.setName(cursor.getString(2));
				item.setDetail(cursor.getString(3));
				item.setPoint_redeem(cursor.getString(4));
				item.setPicture(cursor.getString(5));
				item.setTotal(cursor.getString(6));
				item.setExpired_date(cursor.getString(7));

			} while (cursor.moveToNext());

			Log.v("Hadian", "User -" + item.getName());
		} else {
			item = null;
		}
		db.close();

		// return contact list
		return item;
	}

	public List<Item> getAllItem() {
		List<Item> itemList = new ArrayList<Item>();
		// Select All Query
		String query = "Select * FROM " + TABLE_ITEM;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Item item = new Item();
				item.setId(Integer.parseInt(cursor.getString(0)));
				item.setItem_id(cursor.getString(1));
				item.setName(cursor.getString(2));
				item.setDetail(cursor.getString(3));
				item.setPoint_redeem(cursor.getString(4));
				item.setPicture(cursor.getString(5));
				item.setTotal(cursor.getString(6));
				item.setExpired_date(cursor.getString(7));

				// Adding contact to list
				itemList.add(item);
			} while (cursor.moveToNext());
		} else {
			itemList = null;
		}
		db.close();

		// return contact list
		return itemList;
	}

	public void addHistory(History history) {

		ContentValues values = new ContentValues();
		values.put(HISTORY_ID, history.getHistory_id());
		values.put(HISTORY_NAME, history.getName());
		values.put(HISTORY_PICTURE, history.getPicture());
		values.put(HISTORY_TIMESTAMP, history.getTimestamp());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_HISTORY, null, values);
		db.close();

		Log.v("Hadian", "History Added - " + history.getName());
	}

	public void deleteAllHistory() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_HISTORY);
		db.close();
		Log.v("Hadian", "All History Deleted");
	}

	public List<History> getHistory() {
		List<History> itemList = new ArrayList<History>();
		// Select All Query
		String query = "Select * FROM " + TABLE_HISTORY;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				History history = new History();
				history.setId(Integer.parseInt(cursor.getString(0)));
				history.setTimestamp(cursor.getString(1));
				history.setName(cursor.getString(2));
				history.setPicture(cursor.getString(3));

				// Adding contact to list
				itemList.add(history);
			} while (cursor.moveToNext());
		} else {
			itemList = null;
		}
		db.close();

		// return contact list
		return itemList;
	}

	// month
	public void addLeaderboardMonth(LeaderBoard leader) {

		ContentValues values = new ContentValues();
		values.put(LEADERM_FULLNAME, leader.getFullname());
		values.put(LEADERM_CURRENT_POINT, leader.getCurrent_point());
		values.put(LEADERM_LOGIN_ID, leader.getLogin_id());
//		values.put(LEADERM_PHONE, leader.getPhone());
//		values.put(LEADERM_RANK, leader.getRank());
//		values.put(LEADERM_HIGHFIVE, leader.getHighFive());
//		values.put(LEADERM_H5STATUS, leader.getHiStatus());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_LEADER_MONTH, null, values);
		db.close();

		Log.v("Hadian", "Leader Added - " + leader.getFullname());
	}

	public void deleteAllLeaderboardMonth() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_LEADER_MONTH);
		db.close();
		Log.v("Hadian", "All Leaderboard Deleted");
	}

	public List<LeaderBoard> getLeaderboardMonth() {
		List<LeaderBoard> itemList = new ArrayList<LeaderBoard>();
		// Select All Query
		String query = "Select * FROM " + TABLE_LEADER_MONTH;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				LeaderBoard leader = new LeaderBoard();
				leader.setId(Integer.parseInt(cursor.getString(0)));
				leader.setFullname(cursor.getString(1));
				leader.setLogin_id(cursor.getString(2));
				leader.setCurrent_point(cursor.getString(3));
//				leader.setPhone(cursor.getString(4));
//				leader.setRank(cursor.getString(5));
//				leader.setHighFive(cursor.getString(6));
//				leader.setHiStatus(cursor.getString(7));

				// Adding contact to list
				itemList.add(leader);
			} while (cursor.moveToNext());
		} else {
			itemList = null;
		}
		db.close();

		// return contact list
		return itemList;
	}
	

	public LeaderBoard getLeaderboardMonthById(String viewedId){
		
		String query = "Select * FROM " + TABLE_LEADER_MONTH + " WHERE " + ITEM_ID
				+ " =  \"" + viewedId + "\"";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		LeaderBoard viewedLeader = new LeaderBoard();
		if (cursor.moveToFirst()) {
			do {
				viewedLeader.setId(Integer.parseInt(cursor.getString(0)));
				viewedLeader.setFullname(cursor.getString(1));
				viewedLeader.setLogin_id(cursor.getString(2));
				viewedLeader.setCurrent_point(cursor.getString(3));
//				viewedLeader.setPhone(cursor.getString(4));
//				viewedLeader.setRank(cursor.getString(5));
//				viewedLeader.setHighFive(cursor.getString(6));
//				viewedLeader.setHiStatus(cursor.getString(7));

				// Adding contact to list
			} while (cursor.moveToNext());
		} else {
			viewedLeader = null;
		}
		db.close();

		// return contact list
		return viewedLeader;
	}

	// week
	public void addLeaderboardWeek(LeaderBoard leader) {

		ContentValues values = new ContentValues();
		values.put(LEADERW_FULLNAME, leader.getFullname());
		values.put(LEADERW_CURRENT_POINT, leader.getCurrent_point());
		values.put(LEADERW_LOGIN_ID, leader.getLogin_id());
//		values.put(LEADERW_PHONE, leader.getPhone());
//		values.put(LEADERW_RANK, leader.getRank());
//		values.put(LEADERW_HIGHFIVE, leader.getHighFive());
//		values.put(LEADERW_H5STATUS, leader.getHiStatus());

		SQLiteDatabase db = this.getWritableDatabase();

		db.insert(TABLE_LEADER_WEEK, null, values);
		db.close();

		Log.v("Hadian", "Leader Added - " + leader.getFullname());
	}

	public void deleteAllLeaderboardWeek() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_LEADER_WEEK);
		db.close();
		Log.v("Hadian", "All Leaderboard Deleted");
	}

	public List<LeaderBoard> getLeaderboardWeek() {
		List<LeaderBoard> itemList = new ArrayList<LeaderBoard>();
		// Select All Query
		String query = "Select * FROM " + TABLE_LEADER_WEEK;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				LeaderBoard leader = new LeaderBoard();
				leader.setId(Integer.parseInt(cursor.getString(0)));
				leader.setFullname(cursor.getString(1));
				leader.setLogin_id(cursor.getString(2));
				leader.setCurrent_point(cursor.getString(3));
//				leader.setPhone(cursor.getString(4));
//				leader.setRank(cursor.getString(5));
//				leader.setHighFive(cursor.getString(6));
//				leader.setHiStatus(cursor.getString(7));

				// Adding contact to list
				itemList.add(leader);
			} while (cursor.moveToNext());
		} else {
			itemList = null;
		}
		db.close();

		// return contact list
		return itemList;
	}

}
