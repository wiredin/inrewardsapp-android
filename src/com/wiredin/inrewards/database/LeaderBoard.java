package com.wiredin.inrewards.database;

public class LeaderBoard {
	private int _id;
	private String fullname;
	private String current_point;
	private String login_id;
	private String phone;
	private String rank;
	private String high_five;
	private String h5_status;

	public LeaderBoard() {
		
	}
	
	public LeaderBoard(int id, String fullname, String current_point, String login_id) {
		this._id = id;
		this.fullname = fullname;
		this.current_point = current_point;
		this.login_id = login_id;
	}
	
	public LeaderBoard(String fullname, String current_point, String login_id) {
		this.fullname = fullname;
		this.current_point = current_point;
		this.login_id = login_id;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setFullname(String text){
		this.fullname = text;
	}
	public void setCurrent_point(String text){
		this.current_point = text;
	}
	public void setLogin_id(String text){
		this.login_id = text;
	}
//	public void setPhone(String text){
//		this.phone = text;
//	}
//	public void setRank(String text){
//		this.rank = text;
//	}
//	public void setHighFive(String text){
//		this.high_five = text;
//	}
//	public void setHiStatus(String text){
//		this.h5_status = text;
//	}
	
	
	public int getId(){
		return this._id;
	}
	public String getFullname(){
		return this.fullname;
	}
	public String getCurrent_point(){
		return this.current_point;
	}
	public String getLogin_id(){
		return this.login_id;
	}
//	public String getPhone(){
//		return this.phone;
//	}
//	public String getRank(){
//		return this.rank;
//	}
//	public String getHighFive(){
//		return this.high_five;
//	}
//	public String getHiStatus(){
//		return this.h5_status;
//	}
	

}