package com.wiredin.inrewards.database;

public class History {
	private int _id;
	private String history_id;
	private String name;
	private String picture;
	private String timestamp;

	public History() {
		
	}
	
	public History(int id, String history_id, String name, String picture,
			 String timestamp) {
		this._id = id;
		this.history_id = history_id;
		this.name = name;
		this.picture = picture;
		this.timestamp = timestamp;
	}
	
	public History(String history_id, String name, String picture, String timestamp) {
		this.history_id = history_id;
		this.name = name;
		this.picture = picture;
		this.timestamp = timestamp;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setHistory_id(String text){
		this.history_id = text;
	}
	public void setName(String text){
		this.name = text;
	}
	public void setPicture(String text){
		this.picture = text;
	}
	public void setTimestamp(String text){
		this.timestamp = text;
	}
	
	
	public int getId(){
		return this._id;
	}
	public String getHistory_id(){
		return this.history_id;
	}
	public String getName(){
		return this.name;
	}
	public String getPicture(){
		return this.picture;
	}
	public String getTimestamp(){
		return this.timestamp;
	}

}