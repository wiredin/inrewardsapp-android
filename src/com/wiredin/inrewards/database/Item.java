package com.wiredin.inrewards.database;

public class Item {
	private int _id;
	private String item_id;
	private String name;
	private String detail;
	private String point_redeem;
	private String picture;
	private String total;
	private String expired_date;

	public Item() {
		
	}
	
	public Item(int id, String item_id, String name, String detail, 
			String point_redeem, String picture, String total, String expired_date) {
		this._id = id;
		this.item_id = item_id;
		this.name = name;
		this.detail = detail;
		this.point_redeem = point_redeem;
		this.picture = picture;
		this.total = total;
		this.expired_date = expired_date;
	}
	
	public Item(String item_id, String name, String detail, 
			String point_redeem, String picture, String total, String expired_date) {
		this.item_id = item_id;
		this.name = name;
		this.detail = detail;
		this.point_redeem = point_redeem;
		this.picture = picture;
		this.total = total;
		this.expired_date = expired_date;
	}
	
	public void setId(int id) {
		this._id = id;
	}
	public void setItem_id(String text){
		this.item_id = text;
	}
	public void setName(String text){
		this.name = text;
	}
	public void setDetail(String text){
		this.detail = text;
	}
	public void setPoint_redeem(String text) {
		this.point_redeem = text;
	}
	public void setPicture(String text){
		this.picture = text;
	}
	public void setTotal(String text){
		this.total = text;
	}
	public void setExpired_date(String text){
		this.expired_date = text;
	}
	
	
	public int getId(){
		return this._id;
	}
	public String getItem_id(){
		return this.item_id;
	}
	public String getName(){
		return this.name;
	}
	public String getDetail(){
		return this.detail;
	}
	public String getPoint_redeem(){
		return this.point_redeem;
	}
	public String getPicture(){
		return this.picture;
	}
	public String getTotal(){
		return this.total;
	}
	public String getExpired_date(){
		return this.expired_date;
	}

}