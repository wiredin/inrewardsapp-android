package com.wiredin.inrewards;

import com.actionbarsherlock.app.SherlockActivity;
import com.wiredin.inrewards.database.DatabaseHandler;
import com.wiredin.inrewards.database.User;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ScannedActivity extends SherlockActivity {
	
	String value;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			value = extras.getString("point");
			Log.d("Hadian", "value recieve: " + value);
		}
		
		setContentView(R.layout.activity_scanned);
		
		TextView point = (TextView) findViewById(R.id.scanPoint);
		
		point.setText(value);
		
		if (value != "0"){
			DatabaseHandler dbHandler = new DatabaseHandler(this, null, null, 2);
			User user = dbHandler.getUser();
			
			dbHandler.deleteUser();
			
			int newPoint = Integer.parseInt(value)+Integer.parseInt(user.getCurrent_point());
			
			String ipoint = Integer.toString(newPoint);
			
			User updateUser = new User(user.getUserid(),user.getFullname(),
					user.getAddress(),user.getPhone(),ipoint,user.getRank(),user.getHigh_Five());
			
			dbHandler.addUser(updateUser);
		}
	}


}
